# -*- coding: utf-8 -*-
{
    'name': "library_app",
    'summary': """Manage library catalog and book lending""",
    'author': "Vo The Bao",
    'category': 'Services/Library',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/book_view.xml',
        'views/book_list_templates.xml',
    ],
    'demo': [
        "data/res.partner.csv",
        "data/library.book.csv",
        "data/book_demo.xml",
    ],
    'application': True,
}
