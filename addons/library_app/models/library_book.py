# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import timedelta, datetime


class Book(models.Model):
    _name = 'library.book'
    _description = 'Book'

    name = fields.Char(
        "Title",
        default=None,
        help="Book cover title.",
        readonly=False,
    )
    isbn = fields.Char(help="User a valid ISBN-13 or ISBN-10 ")
    active = fields.Boolean("Active ?", default=True)
    date_published = fields.Date()
    publisher_id = fields.Many2one(
        "res.partner", string="Publisher", index=True)
    author_ids = fields.Many2many("res.partner", string="Authors")
    image = fields.Binary("Cover")
    book_type = fields.Selection(
        [("paper", "Paperback"),
         ("hard", "Hardcover"),
         ("electronic", "Electronic"),
         ("other", "Other")],
        "Type")
    notes = fields.Text("Internal Notes")
    descr = fields.Html("Description")
    copies = fields.Integer(default=1)
    avg_rating = fields.Float("Average Rating", (3, 2))
    price = fields.Monetary("Price", "currency_id")
    currency_id = fields.Many2one("res.currency")
    last_borrow_date = fields.Datetime(
        "Last Borrowed On",
        default=lambda self: fields.Datetime.now())
    expiry_date = fields.Date('Expiry Date')
    days_to_expire = fields.Integer(
        'Days to Expire',
        compute='_compute_days_to_expire')

    def button_check_isbn(self):
        self.ensure_one()
        digit = [int(x) for x in self.isbn if x.isdigit()]
        sum_digit = sum(digit)
        print("Function")
        if sum_digit < 10:
            raise ValidationError("Sum digit is too low: %d" % sum_digit)

    @api.depends('expiry_date')
    def _compute_days_to_expire(self):
        today = fields.Date.context_today(self)
        print(today)
        for book in self:
            if book.expiry_date:
                book.days_to_expire = 110
                expiry_date = datetime.strptime(str(book.expiry_date), '%Y-%m-%d')
            #     book.days_to_expire = (expiry_date - today).days
            else:
                book.days_to_expire = 0